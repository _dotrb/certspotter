+ # Description :
- This is a simple and awful bash code XD written by me. By giving a target domain to the tool it will query the Certspotter api then parse the json data to a readable data using JQ and sort it uniquely to Httpx tool which is going to resolve those subdomain and list the alive ones for you .

+ # Requirements :
+ JQ : run this command in your terminal :
```
sudo apt-get install jq -y
```   
+ httpx : [Tool link](https://github.com/projectdiscovery/httpx)

+ # Installation and usage :
```
git clone https://gitlab.com/iam1evi/certspotter
cd certspotter
chmod +x certspotter.sh
./certspotter
```

